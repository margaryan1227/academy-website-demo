# server 
# Back-end Project Set Up

## Stage 1 (Cloning from git)
 Run following commands to clone from github.

    git clone https://github.com/learnwithada/web-js.git

## Stage 2 (Add .env file)
 Create .env file in ./server/src/envs directory with such content
    
    PORT=8000
    DB_URI=
    JWT_KEY=
    ACCESS_TIME=2m
    REFRESH_TIME=8h
    REFRESH_KEY=
    SALT=
    CLIENT_HOST=http://localhost:3000
    DB_NAME=

## Stage 3 (Installing node modules, and running project on server);
 Run following commands to set up project.

    npm install
    npm run prettify
    npm run lint
    npm run dev 

## Requirements 

    Node JS v16.14.0
    TypeScript v4.6.2
    
## Installing MongoDB

You must have [Homebrew](https://brew.sh/) installed first, then:

````sh
brew tap mongodb/brew
brew update
brew install mongodb-community@6.0
````

To then start MongoDB:

`brew services start mongodb-community@6.0`

To stop MongoDB:

`brew services stop mongodb-community@6.0`

Source: https://www.mongodb.com/docs/manual/tutorial/install-mongodb-on-os-x/

You should then be able to use a tool like [MongoDB Compass](https://www.mongodb.com/products/compass) to connect to your local instance on `mongodb://localhost:27017`.




