const crypto = require('crypto');
const salt = process.env.SALT;
const MIGRATION_NAME = 'add-admin-user';
const ADMIN_PASSWORD = 'Secret123!';
module.exports = {
    async up(db) {
        try {
            console.log(`Starting migration ${MIGRATION_NAME}`);
            const pass = crypto
                .pbkdf2Sync(ADMIN_PASSWORD, salt, 1000, 64, `sha512`)
                .toString(`hex`);
            await db.collection('users').insertOne({
                firstName: 'Super',
                lastName: 'Admin',
                email: 'admin@gmail.com',
                role: 'admin',
                password: pass,
            });
            console.log(`Migration ${MIGRATION_NAME} completed successfully`);
        } catch (err) {
            console.log(`Migration ${MIGRATION_NAME} failed:`, err.message);
        }
    },
};
