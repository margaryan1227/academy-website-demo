import { NextFunction, Response, Request } from 'express';
import BusinessService from '../services/businessService';
import { ApiResponse } from '../core/models/responseModel';
import UserService from '../services/userService';
import GroupService from '../services/groupService';
import { RoleTypes } from '../utils/constant';
import Jwt from '../services/jwtService';
import { isBusinessAdmin } from '../utils/helpers';

const jwtService = new Jwt();
const groupService = new GroupService();
const businessService = new BusinessService();
const userService = new UserService();
export default class BusinessController {
    public async changeUserStatus(
        req: Request,
        res: Response,
        next: NextFunction,
    ): Promise<Response | void> {
        try {
            const user = res.locals.user;
            const { businessId, userId, status } = req.body;
            if (!businessId || !userId || !status) {
                return res
                    .status(400)
                    .send(ApiResponse.generateBadRequestErrorResponse());
            }
            const businessAdmin = await businessService.getBusinessAdmin(
                businessId,
                next,
            );
            if (
                isBusinessAdmin(user.userId, businessAdmin as string, user.role)
            ) {
                return res
                    .status(400)
                    .send(ApiResponse.generateBadRequestErrorResponse());
            }
            const data = await businessService.changeUserStatus(
                businessId,
                userId,
                status,
                next,
            );
            return res
                .status(200)
                .send(
                    new ApiResponse(
                        200,
                        data,
                        'User status changed successfully.',
                        false,
                    ),
                );
        } catch (err) {
            return next(err);
        }
    }

    public async getUserBusinessesById(
        req: Request,
        res: Response,
        next: NextFunction,
    ): Promise<Response | void> {
        try {
            const { userId } = req.params;
            if (!userId) {
                return res
                    .status(400)
                    .send(ApiResponse.generateBadRequestErrorResponse());
            }
            const data = await businessService.getUserBusinessesById(
                userId,
                next,
            );
            if (!data) {
                return res
                    .status(404)
                    .send(
                        ApiResponse.generateNotFoundErrorResponse(
                            'User on Business',
                        ),
                    );
            }
            return res
                .status(200)
                .send(new ApiResponse(200, data, 'Users businesses.', false));
        } catch (err) {
            return next(err);
        }
    }

    public async updateName(
        req: Request,
        res: Response,
        next: NextFunction,
    ): Promise<Response | void> {
        try {
            const { userId, role } = res.locals.user;
            const { _id, name } = req.body;
            if (!_id || !name) {
                return res
                    .status(400)
                    .send(ApiResponse.generateBadRequestErrorResponse());
            }
            const businessAdmin = await businessService.getBusinessAdmin(
                _id,
                next,
            );

            if (isBusinessAdmin(userId, businessAdmin as string, role))
                return res
                    .status(400)
                    .send(ApiResponse.generateBadRequestErrorResponse());
            const data = await businessService.updateNameById(_id, name, next);
            if (!data) {
                return res
                    .status(404)
                    .send(
                        ApiResponse.generateNotFoundErrorResponse('Business'),
                    );
            }
            data.name = name;
            return res
                .status(200)
                .send(
                    new ApiResponse(
                        200,
                        data,
                        'Business name updated successfully.',
                        false,
                    ),
                );
        } catch (err) {
            return next(err);
        }
    }

    public async getAll(
        req: Request,
        res: Response,
        next: NextFunction,
    ): Promise<Response | void> {
        try {
            const { userId } = req.query;
            const { role } = res.locals.user;
            const data =
                role === RoleTypes.ADMIN.toString()
                    ? await businessService.getAll(next)
                    : await businessService.getAll(next, userId as string);
            return res
                .status(200)
                .send(new ApiResponse(200, data, 'Business data.', false));
        } catch (err) {
            return next(err);
        }
    }

    public async inviteUser(
        req: Request,
        res: Response,
        next: NextFunction,
    ): Promise<Response | void> {
        const { businessId, email } = req.body;
        const { userId, role } = res.locals.user;

        if (!businessId || !email) {
            return res
                .status(400)
                .send(ApiResponse.generateBadRequestErrorResponse());
        }
        const businessAdmin = await businessService.getBusinessAdmin(
            businessId,
            next,
        );
        if (isBusinessAdmin(userId, businessAdmin as string, role)) {
            return res
                .status(400)
                .send(ApiResponse.generateBadRequestErrorResponse());
        }
        const data = await businessService.inviteUser(email, businessId, next);
        const message = data
            ? 'User invitation sent successfully.'
            : 'User already exists.';
        return res.status(200).send(new ApiResponse(200, data, message, false));
    }

    public async createBusiness(
        req: Request,
        res: Response,
        next: NextFunction,
    ): Promise<Response | void> {
        try {
            const { name } = req.body;
            const { userId } = req.query;
            if (!name || !userId) {
                return res
                    .status(400)
                    .send(ApiResponse.generateBadRequestErrorResponse());
            }
            const user = await userService.getUserById(
                req,
                userId as string,
                next,
            );
            if (!user) {
                return res
                    .status(404)
                    .send(ApiResponse.generateNotFoundErrorResponse('Creator'));
            }
            const data = await businessService.createBusiness(
                name,
                userId as string,
                next,
            );
            if (user.role === RoleTypes.USER) {
                await userService.updateUserById(
                    user.id,
                    { ...user, role: RoleTypes.BUSINESS_ADMIN },
                    next,
                );
                const token = jwtService.generateAccessToken(
                    user.email,
                    user.id,
                    RoleTypes.BUSINESS_ADMIN,
                );
                return res.status(201).send(
                    new ApiResponse(
                        201,
                        {
                            data: data,
                            token: token,
                        },
                        'Business created successfully.',
                        false,
                    ),
                );
            }
            return res
                .status(201)
                .send(
                    new ApiResponse(
                        201,
                        data,
                        'Business created successfully.',
                        false,
                    ),
                );
        } catch (err) {
            return next(err);
        }
    }

    public async getBusinessById(
        req: Request,
        res: Response,
        next: NextFunction,
    ): Promise<Response | void> {
        try {
            const { role } = res.locals.user;
            const { businessId, userId } = req.query;

            if (!businessId) {
                return res
                    .status(400)
                    .send(ApiResponse.generateBadRequestErrorResponse());
            }
            const data = await businessService.getBusinessById(
                businessId as string,
                next,
            );

            if (!data) {
                return res
                    .status(404)
                    .send(
                        ApiResponse.generateNotFoundErrorResponse('Business'),
                    );
            }
            if (
                role === RoleTypes.BUSINESS_ADMIN.toString() &&
                data.creator.toString().split('"').slice(-2)[0] !== userId
            ) {
                return res
                    .status(400)
                    .send(ApiResponse.generateBadRequestErrorResponse());
            }
            return res
                .status(200)
                .send(new ApiResponse(200, data, 'Data of business.', false));
        } catch (err) {
            return next(err);
        }
    }

    public async deleteBusiness(
        req: Request,
        res: Response,
        next: NextFunction,
    ): Promise<Response | void> {
        try {
            const { userId, role } = res.locals.user;
            const { _id } = req.body;
            if (!_id) {
                return res
                    .status(400)
                    .send(ApiResponse.generateBadRequestErrorResponse());
            }

            const businessAdmin = await businessService.getBusinessAdmin(
                _id,
                next,
            );
            if (!businessAdmin) {
                return res
                    .status(400)
                    .send(ApiResponse.generateBadRequestErrorResponse());
            }
            if (isBusinessAdmin(userId, businessAdmin as string, role)) {
                const data = await businessService.deleteBusinessById(
                    _id,
                    next,
                );
                if (!data) {
                    return res
                        .status(404)
                        .send(
                            ApiResponse.generateNotFoundErrorResponse(
                                'Business',
                            ),
                        );
                }
                for (const groupId of data.groupIds) {
                    await groupService.deleteGroupById(groupId, next);
                }
                return res
                    .status(200)
                    .send(
                        new ApiResponse(
                            200,
                            data,
                            'Business deleted successfully.',
                            false,
                        ),
                    );
            }
            return res
                .status(400)
                .send(ApiResponse.generateBadRequestErrorResponse());
        } catch (err) {
            return next(err);
        }
    }
}
