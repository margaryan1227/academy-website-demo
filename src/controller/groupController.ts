import { NextFunction, Response, Request } from 'express';
import GroupService from '../services/groupService';
import { ApiResponse } from '../core/models/responseModel';
import BusinessService from '../services/businessService';
import { IMember } from '../core/models/businessModel';
import { MemberStatus } from '../utils/constant';

const businessService = new BusinessService();
const groupService = new GroupService();
export default class GroupController {
    public async getGroupProgress(
        req: Request,
        res: Response,
        next: NextFunction,
    ): Promise<Response | void> {
        try {
            const { _id } = req.query;
            const group = await groupService.getGroupById(_id as string, next);
            if (!_id || !group) {
                return res
                    .status(404)
                    .send(ApiResponse.generateNotFoundErrorResponse('Group'));
            }
            const data = await groupService.getGroupProgress(
                _id as string,
                next,
            );
            if (!data) {
                return res
                    .status(400)
                    .send(ApiResponse.generateBadRequestErrorResponse());
            }
            return res
                .status(200)
                .send(
                    new ApiResponse(
                        200,
                        data,
                        'Progress of current Group.',
                        false,
                    ),
                );
        } catch (err) {
            return next(err);
        }
    }
    public async updateGroup(
        req: Request,
        res: Response,
        next: NextFunction,
    ): Promise<Response | void> {
        try {
            const { name, action, userId, courseId, _id } = req.body;
            if (!_id || !action) {
                return res
                    .status(400)
                    .send(ApiResponse.generateBadRequestErrorResponse());
            }
            const validUser = await businessService.getUserBusinessesById(
                userId,
                next,
            );
            if (!validUser || validUser['length'] === 0) {
                return res
                    .status(400)
                    .send(ApiResponse.generateBadRequestErrorResponse());
            }
            const verify = (validUser[0]['members'] as IMember[]).find(
                (item) => {
                    return (
                        JSON.stringify(item.userId) ===
                            JSON.stringify(userId) &&
                        item.status === MemberStatus.INVITED
                    );
                },
            );
            if (verify) {
                return res
                    .status(400)
                    .send(ApiResponse.generateBadRequestErrorResponse());
            }
            const data = await groupService.updateGroup(
                _id,
                name,
                userId,
                courseId,
                action,
                next,
            );
            if (!data) {
                return res
                    .status(404)
                    .send(ApiResponse.generateNotFoundErrorResponse('Group'));
            }
            return res
                .status(200)
                .send(
                    new ApiResponse(
                        200,
                        data,
                        'Group updated successfully',
                        false,
                    ),
                );
        } catch (err) {
            return next(err);
        }
    }
    public async createGroup(
        req: Request,
        res: Response,
        next: NextFunction,
    ): Promise<Response | void> {
        try {
            const creator = req.body.creator ? req.body.creator : null;
            const name = req.body.name ? req.body.name : null;
            const userIds = req.body.userIds ? req.body.userIds : [];
            const courseIds = req.body.courseIds ? req.body.courseIds : [];
            const businessId = req.body.businessId ? req.body.businessId : null;
            const data = await groupService.createGroup(
                name,
                creator,
                businessId,
                userIds,
                courseIds,
                next,
            );
            if (!name || !data || !creator || !businessId) {
                return res
                    .status(400)
                    .send(ApiResponse.generateBadRequestErrorResponse());
            }
            await businessService.addGroupToBusiness(
                businessId,
                data._id,
                next,
            );
            return res
                .status(201)
                .send(
                    new ApiResponse(
                        201,
                        data,
                        'Group created successfully.',
                        false,
                    ),
                );
        } catch (err) {
            return next(err);
        }
    }
    public async getGroup(
        req: Request,
        res: Response,
        next: NextFunction,
    ): Promise<Response | void> {
        try {
            const { _id } = req.body;
            const data = await groupService.getGroupById(_id, next);
            if (!_id) {
                return res
                    .status(400)
                    .send(ApiResponse.generateBadRequestErrorResponse());
            }

            if (!data) {
                return res
                    .status(404)
                    .send(ApiResponse.generateNotFoundErrorResponse('Group'));
            }
            return res
                .status(200)
                .send(new ApiResponse(200, data, null, false));
        } catch (err) {
            return next(err);
        }
    }
    public async getAllGroups(
        req: Request,
        res: Response,
        next: NextFunction,
    ): Promise<Response | void> {
        try {
            const data = await groupService.getAll(next);
            if (!data) {
                return res
                    .status(404)
                    .send(ApiResponse.generateNotFoundErrorResponse('Group'));
            }
            return res
                .status(200)
                .send(new ApiResponse(200, data, 'All groups data', false));
        } catch (err) {
            return next(err);
        }
    }
    public async deleteGroup(
        req: Request,
        res: Response,
        next: NextFunction,
    ): Promise<Response | void> {
        const { _id } = req.body;
        if (!_id) {
            return res
                .status(400)
                .send(ApiResponse.generateBadRequestErrorResponse());
        }
        const data = await groupService.deleteGroupById(_id, next);
        await businessService.removeGroup(_id, next);
        if (!data) {
            return res
                .status(404)
                .send(ApiResponse.generateNotFoundErrorResponse('Group'));
        }
        return res
            .status(200)
            .send(
                new ApiResponse(
                    200,
                    data,
                    'Group deleted successfully.',
                    false,
                ),
            );
    }
}
