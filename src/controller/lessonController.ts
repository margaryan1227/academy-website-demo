import { IAddLesson } from '../core/models/lessonModel';
import Lesson from '../services/lessonService';
import { ApiResponse } from '../core/models/responseModel';
import type { NextFunction, Request, Response } from 'express';
const lessonService = new Lesson();

export default class LessonController {
    public async getLesson(
        req: Request,
        res: Response,
        next: NextFunction,
    ): Promise<Response | void> {
        const { id } = req.params;
        try {
            const lesson = await lessonService.getLessonById(id, next);
            if (!lesson) {
                return res
                    .status(404)
                    .send(ApiResponse.generateNotFoundErrorResponse('Lesson'));
            }
            return res
                .status(200)
                .send(new ApiResponse(200, lesson, 'Lesson data', false));
        } catch (error) {
            return next(error);
        }
    }
    public async deleteLesson(
        req: Request,
        res: Response,
        next: NextFunction,
    ): Promise<Response | void> {
        const { id } = req.params;
        try {
            const lesson = await lessonService.deleteLessonById(id, next);
            if (!lesson) {
                return res
                    .status(404)
                    .send(ApiResponse.generateNotFoundErrorResponse('Lesson'));
            }
            return res
                .status(200)
                .send(
                    new ApiResponse(
                        200,
                        null,
                        'Lesson deleted successfully',
                        false,
                    ),
                );
        } catch (error) {
            return next(error);
        }
    }
    public async getAll(
        _req: Request,
        res: Response,
        next: NextFunction,
    ): Promise<Response | void> {
        try {
            const lessons = await lessonService.getAll(next);
            if (!lessons) {
                return res
                    .status(404)
                    .send(ApiResponse.generateNotFoundErrorResponse('Lessons'));
            }
            return res
                .status(200)
                .send(new ApiResponse(200, lessons, 'Lessons data', false));
        } catch (error) {
            return next(error);
        }
    }
    public async create(
        req: Request,
        res: Response,
        next: NextFunction,
    ): Promise<Response | void> {
        try {
            const { userId } = res.locals.user;
            const { formData } = req.body;
            const {
                topicId,
                instruction,
                instructionFile,
                assessmentQuestion,
                answerChoices,
                simpleAnswer,
            } = JSON.parse(formData);
            if (
                !topicId ||
                !instruction ||
                !assessmentQuestion ||
                (!answerChoices && typeof simpleAnswer !== 'boolean')
            ) {
                return res
                    .status(400)
                    .send(ApiResponse.generateBadRequestErrorResponse());
            }
            let choices = [];
            if (answerChoices && answerChoices?.length) {
                choices = [...answerChoices];
            } else if (typeof simpleAnswer === 'boolean') {
                choices = [
                    {
                        answerChoice: true,
                        isCorrect: simpleAnswer === true,
                    },
                    {
                        answerChoice: false,
                        isCorrect: simpleAnswer === false,
                    },
                ];
            }
            const lesson = await lessonService.addNewLesson(
                {
                    topicId,
                    assessmentQuestion,
                    answerChoices: choices,
                    instruction,
                    instructionFile,
                    createdByUserId: userId,
                },
                next,
            );
            if (lesson) {
                return res
                    .status(200)
                    .send(
                        new ApiResponse(
                            200,
                            lesson,
                            'Lesson successfully saved',
                            false,
                        ),
                    );
            }
        } catch (error) {
            return next(error);
        }
    }
    public async update(
        req: Request,
        res: Response,
        next: NextFunction,
    ): Promise<Response | void> {
        try {
            const { userId } = res.locals.user;
            const { id } = req.params;
            const { formData } = req.body;
            const {
                topicId,
                instruction,
                instructionFile,
                assessmentQuestion,
                answerChoices,
                simpleAnswer,
            } = JSON.parse(formData);
            if (
                !topicId ||
                !instruction ||
                !assessmentQuestion ||
                (!answerChoices && typeof simpleAnswer !== 'boolean')
            ) {
                return res
                    .status(400)
                    .send(ApiResponse.generateBadRequestErrorResponse());
            }
            let choices = [];
            if (answerChoices && answerChoices?.length) {
                choices = [...answerChoices];
            } else if (typeof simpleAnswer === 'boolean') {
                choices = [
                    {
                        answerChoice: true,
                        isCorrect: simpleAnswer === true,
                    },
                    {
                        answerChoice: false,
                        isCorrect: simpleAnswer === false,
                    },
                ];
            }
            const lesson = await lessonService.updateLessonById(
                id,
                {
                    topicId,
                    assessmentQuestion,
                    answerChoices: choices,
                    instruction,
                    instructionFile,
                    createdByUserId: userId,
                } as IAddLesson,
                next,
            );
            if (lesson) {
                return res
                    .status(200)
                    .send(
                        new ApiResponse(
                            200,
                            lesson,
                            'Lesson successfully updated',
                            false,
                        ),
                    );
            }
        } catch (error) {
            return next(error);
        }
    }
}
