import Jwt from '../services/jwtService';
import User from '../services/userService';
import { ApiResponse } from '../core/models/responseModel';
import { UserData } from '../core/models/userModel';
import type { NextFunction, Request, Response } from 'express';
import BusinessService from '../services/businessService';

const businessService = new BusinessService();
const userService = new User();
const jwtService = new Jwt();
export default class UserController {
    public async addUserPassword(
        req: Request,
        res: Response,
        next: NextFunction,
    ): Promise<Response | void> {
        try {
            const { firstName, lastName, password, token } = req.body;
            const user = jwtService.verifyToken(token, process.env.INVITE_KEY);
            if (!user.userId || !firstName || !lastName || !password) {
                return res
                    .status(400)
                    .send(ApiResponse.generateBadRequestErrorResponse());
            }
            const data = await userService.addUserPassword(
                user.userId,
                firstName,
                lastName,
                password,
                next,
            );
            if (!data) {
                return res
                    .status(404)
                    .send(ApiResponse.generateNotFoundErrorResponse('User'));
            }
            const refresh = jwtService.generateRefreshToken(data.email);
            const result = new UserData(
                data.id,
                data.firstName,
                data.lastName,
                data.email,
                [],
                data.role,
                jwtService.generateAccessToken(data.email, data.id, data.role),
                refresh.token,
            );
            await jwtService.saveRefreshTokenToDb(refresh);
            await businessService.deleteUserToken(token, data.id, next);
            return res
                .status(200)
                .send(
                    new ApiResponse(
                        200,
                        result,
                        'User registered successfully.',
                        false,
                    ),
                );
        } catch (err) {
            return next(err);
        }
    }

    public async signIn(
        req: Request,
        res: Response,
        next: NextFunction,
    ): Promise<Response | void> {
        try {
            const { password, email } = req.body;
            if (!email || !password) {
                return res
                    .status(404)
                    .send(ApiResponse.generateBadRequestErrorResponse());
            }
            const user = await userService.getUserByEmail(email, next);
            if (!user) {
                return res.send(
                    new ApiResponse(409, null, 'INVALID_PASSWORD_OR_EMAIL'),
                );
            }
            if (!user.validPassword(password)) {
                return res.send(
                    ApiResponse.generateLoginInvalidErrorResponse(),
                );
            }
            const verify = await jwtService.getDBTokenByEmail(user.email, next);
            if (verify) await jwtService.deleteDbToken(verify.token);
            const refresh = jwtService.generateRefreshToken(user.email);
            const result = new UserData(
                user.id,
                user.firstName,
                user.lastName,
                user.email,
                user.courses,
                user.role,
                jwtService.generateAccessToken(user.email, user.id, user.role),
                refresh.token,
            );
            await jwtService.saveRefreshTokenToDb(refresh);
            return res
                .status(200)
                .send(
                    new ApiResponse(200, result, 'User successfully logged in'),
                );
        } catch (error) {
            return next(error);
        }
    }

    public async editUserRole(
        req: Request,
        res: Response,
        next: NextFunction,
    ): Promise<Response | void> {
        try {
            const { id } = req.params;
            const { role } = req.body;
            if (id) {
                const user = await userService.getUserById(req, id, next);
                if (user) {
                    const updatedUser = await userService.updateUserById(
                        id,
                        { ...user, role },
                        next,
                    );
                    return res
                        .status(200)
                        .send(
                            new ApiResponse(
                                200,
                                updatedUser,
                                'User role updated successfully.',
                            ),
                        );
                }
            }
        } catch (err) {
            return next(err);
        }
    }

    public async signUp(
        req: Request,
        res: Response,
        next: NextFunction,
    ): Promise<Response | void> {
        try {
            const { password, firstName, lastName, email, role } = req.body;
            if (!firstName || !lastName || !email || !password || !role) {
                return res
                    .status(400)
                    .send(ApiResponse.generateBadRequestErrorResponse());
            }
            const possibleUser = await userService.getUserByEmail(email, next);
            if (possibleUser) {
                return res.send(new ApiResponse(409, null, 'EMAIL_EXISTS'));
            }
            const user = await userService.addNewUser(req.body, next);
            if (user) {
                user.token = jwtService.generateAccessToken(
                    user.email,
                    user.id,
                    user.role,
                );
                const refresh = jwtService.generateRefreshToken(user.email);
                user.refreshToken = refresh.token;
                await jwtService.saveRefreshTokenToDb(refresh);
                return res
                    .status(200)
                    .send(
                        new ApiResponse(200, user, 'User successfully saved'),
                    );
            }
        } catch (error) {
            return next(error);
        }
    }

    public async getProfile(
        req: Request,
        res: Response,
        next: NextFunction,
    ): Promise<Response | void> {
        try {
            const { userId, token } = res.locals.user;
            const user = await userService.getUserById(req, userId, next);
            if (!user) {
                res.status(404).send(
                    ApiResponse.generateNotFoundErrorResponse('User'),
                );
            }
            if (user) {
                user.token = token;
            }
            return res
                .status(200)
                .send(new ApiResponse(200, user, 'User data'));
        } catch (error) {
            return next(error);
        }
    }

    public async getUsers(
        _req: Request,
        res: Response,
        next: NextFunction,
    ): Promise<Response | void> {
        try {
            const users = await userService.getAll(next);
            if (!users) {
                return res
                    .status(404)
                    .send(ApiResponse.generateNotFoundErrorResponse('Users'));
            }
            return res
                .status(200)
                .send(new ApiResponse(200, users, 'Users data'));
        } catch (error) {
            return next(error);
        }
    }

    public async getPassedLessons(
        req: Request,
        res: Response,
        next: NextFunction,
    ): Promise<Response | void> {
        try {
            const { userId, courseId } = req.params;
            const response = await userService.getPassedLessons(
                userId,
                courseId,
                next,
            );
            if (response) {
                return res
                    .status(200)
                    .send(
                        new ApiResponse(200, response, 'Users passed lessons'),
                    );
            }
            return res
                .status(404)
                .send(
                    ApiResponse.generateNotFoundErrorResponse(
                        'Users or Courses',
                    ),
                );
        } catch (e) {
            next(e);
        }
    }

    public async signOut(
        req: Request,
        res: Response,
        next: NextFunction,
    ): Promise<Response | void> {
        try {
            const authorizationHeader = req.headers['authorization'];
            const bearer = authorizationHeader.split(' ');
            const refreshToken = bearer[2];
            if (!refreshToken) {
                res.status(400).send(
                    ApiResponse.generateBadRequestErrorResponse(),
                );
            } else {
                await jwtService.deleteDbToken(refreshToken);
                res.status(200).send(
                    new ApiResponse(
                        200,
                        null,
                        'Signed out successfully.',
                        false,
                    ),
                );
            }
        } catch (err) {
            next(err);
        }
    }
}
