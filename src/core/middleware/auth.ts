import type { NextFunction, Response, Request } from 'express';
import User from '../../services/userService';
import Jwt from '../../services/jwtService';
import { ApiResponse } from '../models/responseModel';

const userService = new User();
const jwtService = new Jwt();

export const verifyUser = async (
    req: Request,
    res: Response,
    next: NextFunction,
    // eslint-disable-next-line  @typescript-eslint/no-explicit-any
): Promise<any | void> => {
    try {
        const { userId } = req.query;
        const profile = await userService.getUserById(
            req,
            userId as string,
            next,
        );
        const authorizationHeader = req.headers['authorization'];
        if (!authorizationHeader) {
            return res
                .status(401)
                .send(ApiResponse.generateNotAuthorizedErrorResponse());
        }
        const bearer = authorizationHeader.split(' ');
        //Check if bearer is undefined
        if (
            bearer[0].toLowerCase() !== 'bearer' ||
            typeof bearer[1] === 'undefined' ||
            typeof bearer[2] === 'undefined'
        ) {
            return res
                .status(401)
                .send(ApiResponse.generateBearerInvalidErrorResponse());
        }
        let access_token = bearer[1];
        const refresh_token = bearer[2];
        if (!access_token || !refresh_token) {
            return res
                .status(403)
                .send(ApiResponse.generateNotAuthorizedErrorResponse());
        }
        let user = jwtService.verifyToken(access_token, process.env.JWT_KEY);
        if (!user) {
            const user = jwtService.verifyToken(
                refresh_token,
                process.env.REFRESH_KEY,
            );
            if (!user) {
                return res
                    .status(401)
                    .send(ApiResponse.generateNotAuthorizedErrorResponse());
            } else {
                access_token = jwtService.generateAccessToken(
                    profile['email'] as string,
                    userId as string,
                    profile['role'] as string,
                );
            }
        }
        req.headers[
            'authorization'
        ] = `Bearer ${access_token} ${refresh_token}`;
        user = jwtService.verifyToken(access_token, process.env.JWT_KEY);
        user.token = access_token;
        res.locals.user = user;
        res.header('new', access_token);
        return next();
    } catch (error) {
        return res
            .status(401)
            .send(ApiResponse.generateNotAuthorizedErrorResponse());
    }
};
