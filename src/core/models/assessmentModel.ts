export interface AnswerChoice {
    answerChoice: string | boolean;
    isCorrect: boolean;
}
export interface IAssessment {
    id?: string;
    answerChoices: AnswerChoice[];
    assessmentQuestion: string;
}

export class AssessmentData implements IAssessment {
    constructor(
        public id = '',
        public assessmentQuestion = '',
        public answerChoices = [],
    ) {}
}
