export interface IAssessmentResult {
    id?: string;
    userId: string;
    lessonId: string;
    score: number;
}

export class AssessmentResultData implements IAssessmentResult {
    constructor(
        public id = '',
        public userId = '',
        public lessonId = '',
        public score = 0,
    ) {}
}
