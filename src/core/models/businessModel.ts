import { MemberStatus } from '../../utils/constant';

export interface IMember {
    userId: string;
    token: string;
    status: MemberStatus;
}

export interface IBusiness {
    name: string;
    groupIds: string[];
    creator: string;
    members: IMember[];
}

export class BusinessData implements IBusiness {
    constructor(
        public name: string = '',
        public groupIds: string[] = [],
        public creator: string = '',
        public members: IMember[] = [],
    ) {}
}
