import { CourseVisibility } from '../../utils/constant';

export interface ICourse {
    id?: string;
    name?: string;
    userIds: string[];
    topicIds: string[];
    totalLessons: number;
    createdByUserId: string;
    visibility: CourseVisibility;
    price: string;
    currentPrice: string;
    description: string;
    logo: string;
}

export class CourseData implements ICourse {
    constructor(
        public id = '',
        public name = '',
        public userIds = [],
        public topicIds = [],
        public totalLessons = 0,
        public createdByUserId = '',
        public visibility = CourseVisibility.PUBLIC,
        public price = '',
        public currentPrice = '',
        public description = '',
        public logo = '',
    ) {}
}
