export interface IGroup {
    _id: string;
    name: string;
    creator: string;
    users: string[];
    courses: string[];
}

export class GroupData implements IGroup {
    constructor(
        public _id = '',
        public name = '',
        public creator = '',
        public users = [],
        public courses = [],
    ) {}
}
