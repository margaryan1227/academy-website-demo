export interface IInstruction {
    id?: string;
    instructionType?: string;
    instruction: string;
}

export class InstructionData implements IInstruction {
    constructor(
        public id = '',
        public instructionType = '',
        public instruction = '',
    ) {}
}
