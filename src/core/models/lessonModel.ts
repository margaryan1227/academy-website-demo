import { AnswerChoice } from './assessmentModel';

export interface ILesson {
    id?: string;
    topicId: string;
    instructionId: string[];
    assessmentId: string[];
    instructionFile?: string;
    createdByUserId: string;
}
export interface IAddLesson {
    topicId: string;
    assessmentQuestion?: string;
    answerChoices?: AnswerChoice[];
    instruction?: string;
    instructionFile?: string;
    createdByUserId: string;
}

export class LessonData implements ILesson {
    constructor(
        public id = '',
        public topicId = '',
        public instructionId = [],
        public assessmentId = [],
        public createdByUserId = '',
        public instructionFile = '',
    ) {}
}
