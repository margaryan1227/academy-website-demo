export interface INotification {
    id?: string;
    businessAdminId: string;
    userId: string;
    courseId: string;
    seen: boolean;
    text: string;
    date: string;
}
export class NotificationData implements INotification {
    constructor(
        public id = '',
        public businessAdminId = '',
        public userId = '',
        public courseId = '',
        public seen = false,
        public text = '',
        public date = '',
    ) {}
}
