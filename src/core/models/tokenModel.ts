import { RoleTypes } from '../../utils/constant';

export interface IToken {
    userId: string;
    email: string;
    role: RoleTypes;
    token?: string;
    iat: number;
    exp: number;
}
export interface IRefreshToken {
    id: string;
    email: string;
    token: string;
}
