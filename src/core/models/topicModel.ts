import { LessonData } from './lessonModel';
export interface ITopic {
    id?: string;
    name: string;
    createdByUserId: string;
    lessonIds?: LessonData[];
}

export class TopicData implements ITopic {
    constructor(
        public id = '',
        public name = '',
        public createdByUserId = '',
        public lessonIds = [],
    ) {}
}
