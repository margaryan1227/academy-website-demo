import { CourseData } from './courseModel';
export interface UserInterface {
    id?: string;
    firstName: string;
    lastName: string;
    email: string;
    token: string;
    courses: string[] | (CourseData[] & string[]);
    role: string;
    refreshToken?: string;
    validPassword?: (pass: string) => boolean;
}

export class UserData implements UserInterface {
    constructor(
        public id = '',
        public firstName = '',
        public lastName = '',
        public email = '',
        public courses = [],
        public role = '',
        public token = '',
        public refreshToken = '',
    ) {}
}
