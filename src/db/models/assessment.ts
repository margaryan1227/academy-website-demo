import { Schema, model } from 'mongoose';
import beautifyUnique from 'mongoose-beautiful-unique-validation';
const assessmentSchema = new Schema(
    {
        assessmentQuestion: {
            type: String,
            required: true,
        },
        answerChoices: [
            {
                answerChoice: {
                    type: String,
                    required: true,
                },
                isCorrect: {
                    type: Boolean,
                    required: true,
                },
            },
        ],
    },
    {
        timestamps: {
            createdAt: 'created_at',
            updatedAt: 'updated_at',
        },
    },
);
assessmentSchema.plugin(beautifyUnique, {
    defaultMessage: 'Two choices cannot share the same values ({VALUE})',
});

export default model('Assessment', assessmentSchema);
