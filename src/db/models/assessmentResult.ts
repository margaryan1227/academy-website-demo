import { Schema, model } from 'mongoose';

const { ObjectId } = Schema.Types;
const assessmentResultSchema = new Schema(
    {
        userId: {
            type: ObjectId,
            ref: 'User',
            required: true,
        },
        lessonId: {
            type: ObjectId,
            ref: 'Lesson',
            required: true,
        },
        score: {
            type: Number,
            required: true,
        },
    },
    {
        timestamps: {
            createdAt: 'created_at',
            updatedAt: 'updated_at',
        },
    },
);

export default model('AssessmentResult', assessmentResultSchema);
