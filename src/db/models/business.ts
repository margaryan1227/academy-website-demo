import mongoose, { model } from 'mongoose';

const { Schema } = mongoose;
const { ObjectId } = Schema.Types;
const businessSchema = new Schema(
    {
        name: {
            type: String,
            unique: true,
        },
        members: [
            {
                userId: {
                    type: ObjectId,
                    ref: 'Course',
                },
                status: {
                    type: String,
                    enum: ['invited', 'active', 'inactive'],
                    default: 'invited',
                    required: true,
                },
                token: {
                    type: String,
                    nullable: true,
                    default: null,
                },
            },
        ],
        groupIds: [{ type: ObjectId, ref: 'Group' }],
        creator: {
            type: ObjectId,
            required: true,
        },
    },
    {
        timestamps: {
            createdAt: 'created_at',
            updatedAt: 'updated_at',
        },
    },
);

export default model('Business', businessSchema);
