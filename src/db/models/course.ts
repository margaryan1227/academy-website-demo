import { Schema, model } from 'mongoose';

const { ObjectId } = Schema.Types;
const courseSchema = new Schema(
    {
        name: {
            type: String,
            required: true,
        },
        topicIds: [{ type: ObjectId, ref: 'Topic' }],
        userIds: [{ type: ObjectId, ref: 'User' }],
        totalLessons: {
            type: Number,
            default: 0,
            required: true,
        },
        visibility: {
            type: String,
            enum: ['public', 'private'],
            requires: true,
            default: 'public',
        },
        createdByUserId: {
            type: ObjectId,
            ref: 'User',
            required: true,
        },
        price: {
            type: String,
            required: true,
        },
        currentPrice: {
            type: String,
        },
        description: {
            type: String,
            required: true,
        },
        logo: {
            type: String,
            default: 'default.png',
        },
    },
    {
        timestamps: {
            createdAt: 'created_at',
            updatedAt: 'updated_at',
        },
    },
);

export default model('Course', courseSchema);
