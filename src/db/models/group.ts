import mongoose, { model } from 'mongoose';

const { Schema } = mongoose;
const { ObjectId } = Schema.Types;
const groupSchema = new Schema(
    {
        userIds: [{ type: ObjectId, ref: 'User' }],
        courseIds: [{ type: ObjectId, ref: 'Course' }],
        creator: {
            type: ObjectId,
            required: true,
        },
        name: {
            type: String,
            required: true,
            unique: true,
        },
        businessId: {
            type: ObjectId,
            ref: 'Business',
            required: true,
        },
    },
    {
        timestamps: {
            createdAt: 'created_at',
            updatedAt: 'updated_at',
        },
    },
);

export default model('Group', groupSchema);
