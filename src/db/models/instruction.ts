import { Schema, model } from 'mongoose';
const instructionSchema = new Schema(
    {
        instructionType: {
            type: String,
            enum: ['text', 'video', 'slide'],
            default: 'text',
        },
        instruction: {
            type: String,
            required: true,
        },
    },
    {
        timestamps: {
            createdAt: 'created_at',
            updatedAt: 'updated_at',
        },
    },
);
export default model('Instruction', instructionSchema);
