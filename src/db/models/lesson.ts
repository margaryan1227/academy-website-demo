import { Schema, model } from 'mongoose';

const { ObjectId } = Schema.Types;
const lessonSchema = new Schema(
    {
        topicId: {
            type: ObjectId,
            ref: 'Topic',
            required: true,
        },
        instructionId: [{ type: ObjectId, ref: 'Instruction' }],
        instructionFile: {
            type: String,
        },
        assessmentId: [{ type: ObjectId, ref: 'Assessment' }],
        createdByUserId: {
            type: ObjectId,
            ref: 'User',
            required: true,
        },
    },
    {
        timestamps: {
            createdAt: 'created_at',
            updatedAt: 'updated_at',
        },
    },
);

export default model('Lesson', lessonSchema);
