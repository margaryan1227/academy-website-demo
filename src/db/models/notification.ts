import { Schema, model } from 'mongoose';

const { ObjectId } = Schema.Types;
const notificationSchema = new Schema(
    {
        businessAdminId: {
            type: ObjectId,
            ref: 'User',
            required: true,
        },
        userId: {
            type: ObjectId,
            ref: 'User',
            required: true,
        },
        courseId: {
            type: ObjectId,
            ref: 'Course',
            required: true,
        },
        seen: {
            type: Boolean,
            default: false,
        },
        text: {
            type: String,
        },
    },
    {
        timestamps: {
            createdAt: 'created_at',
            updatedAt: 'updated_at',
        },
    },
);

export default model('Notification', notificationSchema);
