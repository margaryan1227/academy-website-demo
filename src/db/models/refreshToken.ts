import mongoose, { model } from 'mongoose';

const { Schema } = mongoose;
const refreshTokenSchema = new Schema({
    token: {
        type: String,
        unique: true,
    },
    email: {
        type: String,
        unique: true,
    },
    id: {
        type: String,
        unique: true,
    },
});

export default model('RefreshToken', refreshTokenSchema);
