import { Schema, model } from 'mongoose';
import beautifyUnique from 'mongoose-beautiful-unique-validation';

const { ObjectId } = Schema.Types;
const topicSchema = new Schema(
    {
        name: {
            type: String,
            required: true,
            unique: true,
        },
        createdByUserId: {
            type: ObjectId,
            ref: 'User',
        },
        lessonIds: [
            {
                type: ObjectId,
                ref: 'Lesson',
            },
        ],
    },
    {
        timestamps: {
            createdAt: 'created_at',
            updatedAt: 'updated_at',
        },
    },
);
topicSchema.plugin(beautifyUnique, {
    defaultMessage: 'uniqueInstitution',
});
export default model('Topic', topicSchema);
