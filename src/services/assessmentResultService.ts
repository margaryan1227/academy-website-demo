/* eslint-disable @typescript-eslint/no-explicit-any */
import { NextFunction } from 'express';
import AssessmentResult from '../db/models/assessmentResult';
import {
    AssessmentResultData,
    IAssessmentResult,
} from '../core/models/assessmentResult';
import LessonService from './lessonService';
const lessonService = new LessonService();
export default class AssessmentResultService {
    public async getAssessmentResultById(
        id: string,
        next: NextFunction,
    ): Promise<AssessmentResultData | void> {
        try {
            const assessmentResult = await AssessmentResult.findById(id);
            return new AssessmentResultData(
                assessmentResult._id,
                assessmentResult.userId,
                assessmentResult.topicId,
                assessmentResult.score,
            );
        } catch (e) {
            return next(e);
        }
    }

    public async deleteAssessmentResultById(
        id: string,
        next: NextFunction,
    ): Promise<AssessmentResultData | void> {
        try {
            const assessmentResult = await AssessmentResult.findByIdAndDelete(
                id,
            );
            return new AssessmentResultData(
                assessmentResult._id,
                assessmentResult.userId,
                assessmentResult.topicId,
                assessmentResult.score,
            );
        } catch (e) {
            return next(e);
        }
    }

    public async getAssessmentResultsByUser(
        userId: string,
        next: NextFunction,
    ): Promise<AssessmentResultData[] | void> {
        try {
            const assessmentResults = await AssessmentResult.find({ userId });
            return assessmentResults;
        } catch (e) {
            return next(e);
        }
    }
    public async getAll(
        next: NextFunction,
    ): Promise<AssessmentResultData[] | void> {
        try {
            const assessmentResults = await AssessmentResult.find();
            return assessmentResults;
        } catch (e) {
            return next(e);
        }
    }

    public async addNewAssessmentResult(
        { userId, lessonId, score }: IAssessmentResult,
        next: NextFunction,
    ): Promise<AssessmentResultData | void> {
        try {
            const assessmentResult = new AssessmentResult({
                userId,
                lessonId,
                score,
            });
            await assessmentResult.save();
            return new AssessmentResultData(
                assessmentResult._id,
                assessmentResult.userId,
                assessmentResult.lessonId,
                assessmentResult.score,
            );
        } catch (e) {
            return next(e);
        }
    }
    public async getAssessmentResultScore(
        lessonId: string,
        assessmentResult: any[],
        next: NextFunction,
    ): Promise<number | void> {
        try {
            let response = 0;
            for (const key in assessmentResult) {
                const corrects =
                    (await lessonService.getCorrectAnswerByAssessmentQuestion(
                        lessonId,
                        next,
                    )) || [];
                if (
                    corrects.length === 1 &&
                    !Array.isArray(assessmentResult[key])
                ) {
                    assessmentResult[key] === corrects[0].answerChoice &&
                        response++;
                }
                if (
                    corrects.length > 1 &&
                    Array.isArray(assessmentResult[key])
                ) {
                    const answers = corrects.map(
                        ({ answerChoice }) => answerChoice,
                    );
                    const map = {};
                    answers.forEach((i) => (map[i] = false));
                    assessmentResult[key].forEach(
                        (i: string) => map[i] === false && (map[i] = true),
                    );
                    const isMatch = !Object.values(map).includes(false);
                    if (isMatch) {
                        response++;
                    }
                }
            }
            let score = 0;
            if (lessonId) {
                if (!isNaN(+response)) {
                    score = (+response * 100) / 1; // must be updated assessment.length, now we have only one assessment in lesson
                }
                return Math.trunc(score);
            }
        } catch (e) {
            return next(e);
        }
    }
}
