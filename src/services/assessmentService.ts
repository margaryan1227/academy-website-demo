import { AssessmentData, IAssessment } from './../core/models/assessmentModel';
import Assessment from '../db/models/assessment';
import { NextFunction } from 'express';

export default class AssessmentService {
    public async getAssessmentById(
        id: string,
        next: NextFunction,
    ): Promise<AssessmentData> {
        try {
            const assessment = await Assessment.findById(id);
            return new AssessmentData(
                assessment.id,
                assessment.assessmentQuestion,
                assessment.answerChoices,
            );
        } catch (e) {
            next(e);
        }
    }
    public async deleteAssessmentById(
        id: string,
        next: NextFunction,
    ): Promise<AssessmentData | void> {
        try {
            const assessment = await Assessment.findByIdAndDelete(id);
            return new AssessmentData(
                assessment.id,
                assessment.assessmentQuestion,
                assessment.answerChoices,
            );
        } catch (e) {
            return next(e);
        }
    }

    public async updateAssessmentById(
        id: string,
        assessmentData: AssessmentData,
        next: NextFunction,
    ): Promise<AssessmentData | void> {
        try {
            const assessment = await Assessment.findByIdAndUpdate(
                id,
                assessmentData,
                {
                    new: true,
                },
            );
            return assessment;
        } catch (e) {
            return next(e);
        }
    }

    public async addNewAssessment(
        { assessmentQuestion, answerChoices }: IAssessment,
        next: NextFunction,
    ): Promise<AssessmentData | void> {
        try {
            const assessment = new Assessment({
                assessmentQuestion,
                answerChoices,
            });
            await assessment.save();
            return new AssessmentData(
                assessment.id,
                assessment.assessmentQuestion,
                assessment.answerChoices,
            );
        } catch (e) {
            return next(e);
        }
    }
    public async createBulk(
        assessments: IAssessment[],
        next: NextFunction,
    ): Promise<AssessmentData[] | void> {
        try {
            const newAssessments = await Assessment.create(assessments);
            return newAssessments;
        } catch (e) {
            return next(e);
        }
    }
    public async deleteMany(
        query: {
            lessonId: string;
        },
        next: NextFunction,
    ): Promise<{ deletedCount: number } | void> {
        try {
            const newAssessments = await Assessment.deleteMany(query);
            return newAssessments;
        } catch (e) {
            return next(e);
        }
    }
}
