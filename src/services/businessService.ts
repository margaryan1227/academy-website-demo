import Business from '../db/models/business';
import { NextFunction } from 'express';
import { BusinessData, IMember } from '../core/models/businessModel';
import Group from '../db/models/group';
import { InvitedAction, MemberStatus, RoleTypes } from '../utils/constant';
import Jwt from './jwtService';
import UserService from './userService';
import { sendEmail } from '../utils/sendgrid';
// import Notification from './notificationService';
// import { NotificationData } from '../core/models/notificationModel';
// const io = require('socket.io');

const userService = new UserService();
const jwtService = new Jwt();
export default class BusinessService {
    public async getBusinessAdmin(
        businessId: string,
        next: NextFunction,
    ): Promise<string | void> {
        try {
            const business = await Business.findById(businessId);
            return business ? business.creator : business;
        } catch (err) {
            return next(err);
        }
    }
    public async changeUserStatus(
        businessId: string,
        userId: string,
        status: MemberStatus.ACTIVE | MemberStatus.INACTIVE,
        next: NextFunction,
    ): Promise<BusinessData | void> {
        try {
            await Business.findByIdAndUpdate(businessId, {
                members: {
                    userId: userId,
                    status: status,
                    token: null,
                },
            });
            return Business.findById(businessId);
        } catch (err) {
            return next(err);
        }
    }
    public async getUserBusinessesById(
        userId: string,
        next: NextFunction,
    ): Promise<BusinessData[] | void> {
        try {
            return Business.find({
                members: {
                    $elemMatch: {
                        userId: userId,
                    },
                },
            });
        } catch (err) {
            return next(err);
        }
    }
    public async addGroupToBusiness(
        businessId: string,
        groupId: string,
        next: NextFunction,
    ): Promise<BusinessData | void> {
        try {
            return Business.findByIdAndUpdate(businessId, {
                $push: {
                    groupIds: groupId,
                },
            });
        } catch (err) {
            return next(err);
        }
    }
    public async removeGroup(
        id: string,
        next: NextFunction,
    ): Promise<BusinessData[] | void> {
        try {
            return Business.updateMany(
                {},
                {
                    $pullAll: {
                        groupIds: id,
                    },
                },
            ) as unknown as BusinessData[];
        } catch (err) {
            return next(err);
        }
    }
    public async updateNameById(
        businessId: string,
        name: string,
        next: NextFunction,
    ): Promise<BusinessData | void> {
        try {
            return Business.findByIdAndUpdate(businessId, {
                name: name,
            });
        } catch (err) {
            return next(err);
        }
    }
    public async getAll(
        next: NextFunction,
        userId?: string,
    ): Promise<BusinessData[] | void> {
        try {
            if (userId) {
                return Business.find({
                    creator: userId,
                });
            }
            return Business.find();
        } catch (err) {
            return next(err);
        }
    }
    public async inviteUser(
        email: string,
        businessId: string,
        next: NextFunction,
    ): Promise<BusinessData | void> {
        try {
            let status;
            let memberStatus = MemberStatus.ACTIVE;
            let userId;
            const active = await jwtService.getDBTokenByEmail(email, next);
            status = active ? InvitedAction.ACTIVE : InvitedAction.SIGNIN;
            await userService
                .getUserByEmail(email, next)
                .then((user) => (userId = user ? user.id : null))
                .catch((err) => next(err));
            if (!userId) {
                await userService
                    .addNewUser(
                        {
                            firstName: ' ',
                            lastName: ' ',
                            email: email,
                            password: ' ',
                            role: RoleTypes.USER,
                        },
                        next,
                    )
                    .then((user) => (userId = user ? user.id : ''))
                    .catch((err) => next(err));
                status = InvitedAction.SIGNUP;
                memberStatus = MemberStatus.INVITED;
            }
            const verify = await Business.find({
                members: {
                    $elemMatch: {
                        userId: userId,
                    },
                },
            });
            let verified = verify.filter((item) => item._id == businessId);
            let verified2;
            if (verified.length > 0) {
                verified2 = (verified[0]['members'] as IMember[]).find(
                    (item) => {
                        return (
                            JSON.stringify(item.userId) ===
                                JSON.stringify(userId) &&
                            item.status === MemberStatus.INVITED
                        );
                    },
                );
                verified = (verified[0]['members'] as IMember[]).map(
                    (item) =>
                        ((item as unknown as string) = JSON.stringify(
                            item.userId,
                        )),
                );
            }
            if (verified.includes(JSON.stringify(userId))) {
                if (verified2) {
                    await Business.findByIdAndUpdate(businessId, {
                        members: {
                            $pullAll: verified2,
                        },
                    });
                } else {
                    return null;
                }
            }
            const inviteToken = jwtService.generateInviteToken(email, userId);
            const member: IMember = {
                userId: userId,
                status: memberStatus,
                token: inviteToken,
            };
            await sendEmail(
                email,
                'Invitation from Ada Learning',
                `Please, follow the link ${process.env.CLIENT_HOST}/user-businesses?item=${inviteToken}&status=${status}`,
                next,
            );
            // const notification = new Notification();
            // const newNotification = await notification.addNewNotification(
            //     {
            //         businessAdminId: '6282786db830abd57f2910e4',
            //         courseId: '62cd92936291617db9cc6b9e',
            //         userId,
            //     } as NotificationData,
            //     next,
            // );
            // io.emit('getNotification', newNotification);
            return Business.findByIdAndUpdate(businessId, {
                $push: {
                    members: member,
                },
            });
        } catch (err) {
            return next(err);
        }
    }
    public async createBusiness(
        name: string,
        creator: string,
        next: NextFunction,
    ): Promise<BusinessData[] | void> {
        try {
            return Business.create({
                name: name,
                creator: creator,
            });
        } catch (err) {
            return next(err);
        }
    }
    public async deleteUserToken(
        token: string,
        userId: string,
        next: NextFunction,
    ): Promise<BusinessData | void> {
        try {
            return Business.findOneAndUpdate(
                {
                    members: {
                        $elemMatch: {
                            token: token,
                        },
                    },
                },
                {
                    members: {
                        userId: userId,
                        status: 'active',
                        token: null,
                    },
                },
            );
        } catch (err) {
            return next(err);
        }
    }
    public async getBusinessById(
        _id: string,
        next: NextFunction,
    ): Promise<BusinessData | void> {
        try {
            return Business.findById(_id);
        } catch (err) {
            return next(err);
        }
    }
    public async deleteBusinessById(
        _id: string,
        next: NextFunction,
    ): Promise<BusinessData | void> {
        try {
            const deleted = await Business.findByIdAndDelete(_id);
            if (deleted.groupIds && deleted.groupIds.length) {
                for (const groupId of deleted.gorupIds) {
                    await Group.findByIdAndDelete(groupId);
                }
            }
            return deleted;
        } catch (err) {
            return next(err);
        }
    }
}
