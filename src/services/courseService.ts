import Course from '../db/models/course';
import User from '../db/models/user';
import Lesson from '../db/models/lesson';
import { CourseData, ICourse } from '../core/models/courseModel';
import { NextFunction, Request } from 'express';
import mongoose from 'mongoose';
import user from './userService';
import { CourseVisibility, RoleTypes } from '../utils/constant';
import { IToken } from '../core/models/tokenModel';
import Group from '../db/models/group';

const userService = new user();
export default class CourseService {
    public async getCourseById(
        id: string,
        next: NextFunction,
    ): Promise<CourseData | void> {
        try {
            const course = await Course.findById(id);
            return new CourseData(
                course._id,
                course.userIds,
                course.topicIds,
                course.totalLessons,
                course.createdByUserId,
                course.visibility,
                course.price,
                course?.currentPrice,
                course.description,
                course.logo,
            );
        } catch (e) {
            return next(e);
        }
    }
    public async getPublic(next: NextFunction): Promise<CourseData[] | void> {
        try {
            return Course.find({
                visibility: CourseVisibility.PUBLIC,
            });
        } catch (err) {
            return next(err);
        }
    }
    public async deleteCourseById(
        id: string,
        next: NextFunction,
    ): Promise<CourseData | void> {
        const session = await mongoose.startSession();
        try {
            await session.startTransaction();
            const deletedCourse = await Course.findByIdAndDelete(id).session(
                session,
            );
            await User.updateMany(
                {},
                {
                    $pull: { courses: { course: deletedCourse._id } },
                },
                { multi: true },
            ).session(session);
            await Group.updateMany(
                {},
                {
                    $pull: { courseIds: deletedCourse._id },
                },
                { multi: true },
            ).session(session);
            await session.commitTransaction();
            return new CourseData(
                deletedCourse._id,
                deletedCourse.topicIds,
                deletedCourse.userIds,
                deletedCourse.totalLessons,
                deletedCourse.createdByUserId,
                deletedCourse.visibility,
                deletedCourse.price,
                deletedCourse?.currentPrice,
                deletedCourse.description,
                deletedCourse.logo,
            );
        } catch (e) {
            await session.abortTransaction();
            return next(e);
        } finally {
            session.endSession();
        }
    }
    public async getAll(
        req: Request,
        user: IToken,
        next: NextFunction,
    ): Promise<CourseData[] | void> {
        try {
            const { query } = req;
            let courses = await Course.aggregate([
                {
                    $match: Object.assign(
                        user?.role === RoleTypes.BUSINESS_ADMIN
                            ? {
                                  createdByUserId: new mongoose.Types.ObjectId(
                                      user?.userId,
                                  ),
                              }
                            : {},
                        {
                            visibility: CourseVisibility.PRIVATE,
                        },
                        query.search
                            ? {
                                  name: {
                                      $regex: new RegExp(
                                          query.search.toString(),
                                      ),
                                      $options: 'i',
                                  },
                              }
                            : {},
                    ),
                },
                {
                    $sort:
                        query?.sort === 'newest'
                            ? {
                                  created_at: -1,
                              }
                            : { created_at: 1 },
                },
                {
                    $lookup: {
                        from: 'topics',
                        let: { topicIds: '$topicIds' },
                        pipeline: [
                            {
                                $match: {
                                    $expr: { $in: ['$_id', '$$topicIds'] },
                                },
                            },
                        ],
                        as: 'topics',
                    },
                },
                {
                    $lookup: {
                        from: 'users',
                        let: { userIds: '$userIds' },
                        pipeline: [
                            {
                                $match: {
                                    $expr: { $in: ['$_id', '$$userIds'] },
                                },
                            },
                            {
                                $project: {
                                    id: '$_id',
                                    email: 1,
                                    firstName: 1,
                                    lastName: 1,
                                    courses: 1,
                                },
                            },
                            {
                                $project: {
                                    _id: 0,
                                },
                            },
                        ],
                        as: 'users',
                    },
                },
                {
                    $project: {
                        id: '$_id',
                        name: 1,
                        topics: 1,
                        users: 1,
                        totalLessons: 1,
                        createdByUserId: 1,
                        visibility: 1,
                        price: 1,
                        currentPrice: 1,
                        description: 1,
                        logo: 1,
                    },
                },
            ]);
            if (!courses) {
                courses = new Array<CourseData>();
            }
            const publicCourses = await Course.find({
                visibility: CourseVisibility.PUBLIC,
            });
            for (const item of publicCourses) {
                courses.push(item);
            }
            return courses;
        } catch (e) {
            return next(e);
        }
    }

    public async addNewCourse(
        courseData: CourseData,
        next: NextFunction,
    ): Promise<CourseData | void> {
        const session = await mongoose.startSession();
        try {
            await session.startTransaction();
            const newCourse = await Course.create([courseData], { session });
            for (const item of newCourse[0]?.userIds) {
                await User.updateOne(
                    { _id: item },
                    {
                        $push: { courses: newCourse[0]._id },
                    },
                ).session(session);
            }
            const data = await Course.aggregate([
                {
                    $match: {
                        _id: new mongoose.Types.ObjectId(newCourse[0]?._id),
                    },
                },
                {
                    $lookup: {
                        from: 'topics',
                        let: { topicIds: '$topicIds' },
                        pipeline: [
                            {
                                $match: {
                                    $expr: { $in: ['$_id', '$$topicIds'] },
                                },
                            },
                        ],
                        as: 'topics',
                    },
                },
                {
                    $lookup: {
                        from: 'users',
                        let: { userIds: '$userIds' },
                        pipeline: [
                            {
                                $match: {
                                    $expr: { $in: ['$_id', '$$userIds'] },
                                },
                            },
                            {
                                $project: {
                                    id: '$_id',
                                    email: 1,
                                    firstName: 1,
                                    lastName: 1,
                                    courses: 1,
                                },
                            },
                            {
                                $project: {
                                    _id: 0,
                                },
                            },
                        ],
                        as: 'users',
                    },
                },
                {
                    $project: {
                        id: '$_id',
                        name: 1,
                        topics: 1,
                        users: 1,
                        totalLessons: 1,
                        createdByUserId: 1,
                        visibility: 1,
                        price: 1,
                        currentPrice: 1,
                        description: 1,
                        logo: 1,
                    },
                },
            ]).session(session);
            await session.commitTransaction();
            return data[0];
        } catch (e) {
            await session.abortTransaction();
            return next(e);
        } finally {
            session.endSession();
        }
    }
    public async update(
        id: string,
        courseData: {
            name: string;
            userIds: string;
            topicIds: string;
            visibility: CourseVisibility;
            price: number;
            currentPrice: number;
            description: string;
            logo: string;
        } & {
            action: string;
        },
        userId: string,
        next: NextFunction,
    ): Promise<CourseData | void> {
        const session = await mongoose.startSession();
        try {
            await session.startTransaction();

            const ids = {} as {
                name: string;
                userIds: string;
                topicIds: string;
            };
            if (courseData?.userIds) {
                ids.userIds = courseData?.userIds;
            }
            if (courseData?.topicIds) {
                ids.topicIds = courseData?.topicIds;
            }
            const lessons = ids?.topicIds
                ? await Lesson.find({ topicId: ids?.topicIds }).session(session)
                : [];
            const course = await Course.findById(id).session(session);
            if (
                course.visibility === CourseVisibility.PRIVATE &&
                course.createdByUserId.toString().split('"').slice(-2)[0] !==
                    userId
            ) {
                await session.abortTransaction();
                return null;
            }
            let updatedCourse = await Course.findByIdAndUpdate(id, {
                name: courseData.name,
                visibility: courseData.visibility,
                price: courseData.price,
                currentPrice: courseData?.currentPrice,
                description: courseData.description,
                totalLessons: course?.totalLessons + lessons?.length,
                logo: courseData.logo,
            }).session(session);
            updatedCourse =
                courseData?.action === 'add'
                    ? await Course.findByIdAndUpdate(
                          id,
                          {
                              $push: ids,
                          },
                          { session },
                      )
                    : courseData?.action === 'remove'
                    ? await Course.findByIdAndUpdate(
                          id,
                          {
                              $pull: ids,
                          },
                          { session },
                      )
                    : null;
            const status = await userService.getCourseStatus(
                ids.userIds,
                updatedCourse,
                next,
            );
            if (ids?.userIds) {
                courseData?.action === 'remove'
                    ? await User.updateOne(
                          { _id: ids.userIds },
                          {
                              $pull: { courses: { course: id, status } },
                          },
                      ).session(session)
                    : courseData?.action === 'add'
                    ? await User.updateOne(
                          { _id: ids.userIds },
                          {
                              $push: { courses: { course: id, status } },
                          },
                      ).session(session)
                    : null;
            }
            if (ids?.topicIds) {
                if (updatedCourse?.userIds?.length) {
                    const usersToUpdate = await User.find({
                        'courses.course': { $in: updatedCourse?._id },
                    }).session(session);
                    for (const user of usersToUpdate) {
                        for (const course of user?.courses) {
                            const courseObj = await Course.findById(
                                course.course,
                            ).session(session);
                            const status = await userService.getCourseStatus(
                                user._id,
                                courseObj,
                                next,
                            );
                            await User.findOneAndUpdate(
                                {
                                    _id: user._id,
                                    courses: {
                                        $elemMatch: {
                                            course: new mongoose.Types.ObjectId(
                                                courseObj?._id,
                                            ),
                                        },
                                    },
                                },
                                { $set: { 'courses.$.status': status } },
                                { new: true, safe: true },
                            ).session(session);
                        }
                    }
                }
            }
            const data = await Course.aggregate([
                {
                    $match: {
                        _id: new mongoose.Types.ObjectId(updatedCourse?._id),
                    },
                },
                {
                    $lookup: {
                        from: 'topics',
                        let: { topicIds: '$topicIds' },
                        pipeline: [
                            {
                                $match: {
                                    $expr: { $in: ['$_id', '$$topicIds'] },
                                },
                            },
                        ],
                        as: 'topics',
                    },
                },
                {
                    $lookup: {
                        from: 'users',
                        let: { userIds: '$userIds' },
                        pipeline: [
                            {
                                $match: {
                                    $expr: { $in: ['$_id', '$$userIds'] },
                                },
                            },
                            {
                                $project: {
                                    id: '$_id',
                                    email: 1,
                                    firstName: 1,
                                    lastName: 1,
                                    courses: 1,
                                },
                            },
                            {
                                $project: {
                                    _id: 0,
                                },
                            },
                        ],
                        as: 'users',
                    },
                },
                {
                    $project: {
                        name: 1,
                        id: '$_id',
                        topics: 1,
                        users: 1,
                        totalLessons: 1,
                        createdByUserId: 1,
                        visibility: 1,
                        price: 1,
                        currentPrice: 1,
                        description: 1,
                        logo: 1,
                    },
                },
            ]).session(session);
            await session.commitTransaction();
            return data[0];
        } catch (e) {
            await session.abortTransaction();
            return next(e);
        } finally {
            session.endSession();
        }
    }
    public async createBulk(
        lessons: ICourse[],
        next: NextFunction,
    ): Promise<CourseData[] | void> {
        try {
            const newCourses = await Course.create(lessons);
            return newCourses;
        } catch (e) {
            return next(e);
        }
    }
    public async deleteMany(
        query: {
            topicId: string;
        },
        next: NextFunction,
    ): Promise<{ deletedCount: number } | void> {
        try {
            const deletedCourses = await Course.deleteMany(query);
            return deletedCourses;
        } catch (e) {
            return next(e);
        }
    }
}
