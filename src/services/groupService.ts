import Group from '../db/models/group';
import { GroupData } from '../core/models/groupModel';
import { NextFunction } from 'express';
import UserService from './userService';
import { GroupProgressData } from '../core/models/groupProgressModel';
import { CourseStatus } from '../utils/constant';

const userService = new UserService();
export default class GroupService {
    public async getGroupProgress(
        _id: string,
        next: NextFunction,
    ): Promise<GroupProgressData[] | void> {
        try {
            const res: GroupProgressData[] = [];
            const group = await Group.findById(_id);
            for (const user of group.userIds) {
                for (const course of group.courseIds) {
                    const passedLessons = await userService.getPassedLessons(
                        user,
                        course,
                        next,
                    );
                    if (passedLessons) {
                        if (passedLessons.passed == 0) {
                            res.push(
                                new GroupProgressData(
                                    user,
                                    course,
                                    CourseStatus.NOT_STARTED,
                                ),
                            );
                        } else if (
                            passedLessons.passed == passedLessons.total
                        ) {
                            res.push(
                                new GroupProgressData(
                                    user,
                                    course,
                                    CourseStatus.PENDING,
                                ),
                            );
                        } else {
                            res.push(
                                new GroupProgressData(
                                    user,
                                    course,
                                    CourseStatus.COMPLETED,
                                ),
                            );
                        }
                    }
                }
            }
            let arr = [];
            if (res) {
                for (const elem of res) {
                    const num = res.filter(
                        (item) =>
                            item.progress == elem.progress &&
                            item.courseId == elem.courseId,
                    ).length;
                    if (
                        !arr.includes({
                            courseId: elem.courseId,
                            progress: elem.progress,
                            userCount: num,
                        })
                    ) {
                        arr.push({
                            courseId: elem.courseId,
                            progress: elem.progress,
                            userCount: num,
                        });
                    }
                }
                arr = arr.reduce((acc, elem) => {
                    !acc.find(
                        (item) =>
                            item.courseId === elem.courseId &&
                            item.progress === elem.progress,
                    ) && acc.push(elem);
                    return acc;
                }, []);
            }
            return arr;
        } catch (err) {
            return next(err);
        }
    }
    public async updateGroup(
        _id: string,
        name: string,
        userId: string,
        courseId: string,
        action: 'add' | 'remove' | 'rename',
        next: NextFunction,
    ): Promise<GroupData | void> {
        try {
            switch (action) {
                case 'add':
                    await Group.findByIdAndUpdate(
                        {
                            _id,
                        },
                        {
                            $push: {
                                userIds: userId,
                                courseIds: courseId,
                            },
                        },
                    );
                    break;
                case 'remove':
                    await Group.findByIdAndUpdate(
                        {
                            _id,
                        },
                        {
                            $pull: {
                                userIds: userId,
                                courseIds: courseId,
                            },
                        },
                    );
                    break;
                case 'rename':
                    await Group.findByIdAndUpdate(
                        {
                            _id,
                        },
                        {
                            name: name,
                        },
                    );
                    break;
            }
            return Group.findById({ _id: _id });
        } catch (err) {
            return next(err);
        }
    }
    public async createGroup(
        name: string,
        creator: string,
        businessId: string,
        userIds: string[],
        courseIds: string[],
        next: NextFunction,
    ): Promise<GroupData | void> {
        try {
            return Group.create({
                name: name,
                creator: creator,
                businessId: businessId,
                userIds: userIds ? userIds : [],
                courseIds: courseIds ? courseIds : [],
            });
        } catch (err) {
            return next(err);
        }
    }
    public async getAll(next: NextFunction): Promise<Array<GroupData> | void> {
        try {
            return Group.find();
        } catch (err) {
            return next(err);
        }
    }
    public async getGroupById(
        _id: string,
        next: NextFunction,
    ): Promise<GroupData | void> {
        try {
            return Group.findById({ _id: _id });
        } catch (err) {
            return next(err);
        }
    }
    public async deleteGroupById(
        _id: string,
        next: NextFunction,
    ): Promise<GroupData | void> {
        try {
            return Group.findByIdAndDelete({ _id: _id });
        } catch (err) {
            return next(err);
        }
    }
}
