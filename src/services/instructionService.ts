import Instruction from '../db/models/instruction';
import { InstructionData, IInstruction } from '../core/models/instructionModel';
import { NextFunction } from 'express';

export default class InstructionService {
    public async getInstructionById(
        id: string,
        next: NextFunction,
    ): Promise<InstructionData | void> {
        try {
            const instruction = await Instruction.findById(id);
            return new InstructionData(
                instruction._id,
                instruction.instruction,
                instruction.instructionType,
            );
        } catch (e) {
            return next(e);
        }
    }
    public async updateInstructionById(
        id: string,
        instructionData: IInstruction,
        next: NextFunction,
    ): Promise<InstructionData | void> {
        try {
            const instruction = await Instruction.findByIdAndUpdate(
                id,
                instructionData,
            );
            return new InstructionData(
                instruction._id,
                instruction.instruction,
                instruction.instructionType,
            );
        } catch (e) {
            return next(e);
        }
    }
    public async getInstruction(
        inst: string,
        next: NextFunction,
    ): Promise<InstructionData | void> {
        try {
            const instruction = await Instruction.findOne({
                instruction: inst,
            });
            return instruction
                ? new InstructionData(
                      instruction?._id,
                      instruction?.instruction,
                      instruction?.instructionType,
                  )
                : null;
        } catch (e) {
            return next(e);
        }
    }

    public async addNewInstruction(
        { instruction, instructionType = 'text' }: IInstruction,
        next: NextFunction,
    ): Promise<InstructionData | void> {
        try {
            const newInstruction = new Instruction({
                instruction,
                instructionType,
            });
            await newInstruction.save();
            return new InstructionData(
                newInstruction._id,
                newInstruction.instructionType,
                newInstruction.instruction,
            );
        } catch (e) {
            return next(e);
        }
    }
    public async deleteInstructionById(
        id: string,
        next: NextFunction,
    ): Promise<InstructionData | void> {
        try {
            const instruction = await Instruction.findByIdAndDelete(id);
            return new InstructionData(
                instruction._id,
                instruction.instruction,
                instruction.instructionType,
            );
        } catch (e) {
            return next(e);
        }
    }

    public async createBulk(
        instructions: IInstruction[],
        next: NextFunction,
    ): Promise<IInstruction[] | void> {
        try {
            const newInstructions = await Instruction.create(instructions);
            return newInstructions;
        } catch (e) {
            return next(e);
        }
    }
}
