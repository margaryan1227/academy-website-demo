import jwt from 'jsonwebtoken';
import { v4 as uuid } from 'uuid';
import { IRefreshToken, IToken } from '../core/models/tokenModel';
import RefreshToken from '../db/models/refreshToken';
import { NextFunction } from 'express';

export default class Jwt {
    public generateAccessToken(
        email: string,
        userId: string,
        role: string,
    ): string {
        try {
            return jwt.sign({ userId, email, role }, process.env.JWT_KEY, {
                expiresIn: process.env.ACCESS_TIME,
            });
        } catch (e) {
            return null;
        }
    }
    public generateInviteToken(email: string, userId: string): string {
        try {
            return jwt.sign({ userId, email }, process.env.INVITE_KEY);
        } catch (e) {
            return null;
        }
    }
    public generateRefreshToken(email: string): IRefreshToken | null {
        try {
            const payload = {
                id: uuid(),
                email: email,
            };
            const token = jwt.sign(payload, process.env.REFRESH_KEY, {
                expiresIn: process.env.REFRESH_TIME,
            });
            return {
                email: email,
                id: payload.id,
                token: token,
            };
        } catch (err) {
            return null;
        }
    }
    public async saveRefreshTokenToDb(
        token: IRefreshToken,
    ): Promise<IRefreshToken | null> {
        try {
            return RefreshToken.create(token);
        } catch (err) {
            return null;
        }
    }
    public async deleteDbToken(token: string): Promise<IRefreshToken | null> {
        try {
            return RefreshToken.findOneAndDelete({
                token: token,
            });
        } catch (err) {
            return null;
        }
    }
    public verifyToken(token: string, key: string): IToken {
        try {
            return jwt.verify(token, key);
        } catch (error) {
            return null;
        }
    }
    public async getDBTokenByEmail(
        email: string,
        next: NextFunction,
    ): Promise<IToken | void> {
        try {
            return RefreshToken.findOne({ email });
        } catch (err) {
            return next(err);
        }
    }
}
