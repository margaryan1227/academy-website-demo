import Lesson from '../db/models/lesson';
import Instruction from '../db/models/instruction';
import Assessment from '../db/models/assessment';
import Course from '../db/models/course';
import User from '../db/models/user';
import UserService from './userService';
import Topic from '../db/models/topic';
import { LessonData, ILesson, IAddLesson } from '../core/models/lessonModel';
import { NextFunction } from 'express';
import mongoose from 'mongoose';
const userService = new UserService();
export default class LessonService {
    public async getLessonById(
        id: string,
        next: NextFunction,
    ): Promise<LessonData | void> {
        try {
            return Lesson.findById(id);
        } catch (e) {
            return next(e);
        }
    }
    public async deleteLessonById(
        id: string,
        next: NextFunction,
    ): Promise<LessonData | void> {
        const session = await mongoose.startSession();
        try {
            await session.startTransaction();
            const lesson = await Lesson.findById(id).session(session);

            await Topic.findByIdAndUpdate(lesson.topicId, {
                $pull: {
                    lessonIds: id,
                },
            }).session(session);

            if (lesson.instructionId[0]) {
                await Instruction.findByIdAndDelete(
                    lesson.instructionId[0],
                ).session(session);
            }
            if (lesson.assessmentId[0]) {
                await Assessment.findByIdAndDelete(
                    lesson.assessmentId[0],
                ).session(session);
            }
            const deletedLesson = await Lesson.findByIdAndDelete(id).session(
                session,
            );
            const courses = await Course.find({
                topicIds: { $in: [lesson.topicId] },
            }).session(session);
            const courseIds = courses.map(({ _id }) => _id);
            const usersToUpdate = await User.find({
                'courses.course': { $in: courseIds },
            }).session(session);
            for (const user of usersToUpdate) {
                for (const course of user?.courses) {
                    const courseObj = await Course.findById(
                        course.course,
                    ).session(session);
                    const status = await userService.getCourseStatus(
                        user._id,
                        courseObj,
                        next,
                    );
                    await User.findOneAndUpdate(
                        {
                            _id: user._id,
                            courses: {
                                $elemMatch: {
                                    course: new mongoose.Types.ObjectId(
                                        courseObj?._id,
                                    ),
                                },
                            },
                        },
                        { $set: { 'courses.$.status': status } },
                        { new: true, safe: true },
                    ).session(session);
                }
            }
            await session.commitTransaction();
            return new LessonData(
                deletedLesson._id,
                deletedLesson.topicId,
                deletedLesson.instructionId,
                deletedLesson.indtructionFile,
                deletedLesson.assessmentId,
                deletedLesson.createdByUserId,
            );
        } catch (e) {
            await session.abortTransaction();
            return next(e);
        } finally {
            session.endSession();
        }
    }
    public async getCorrectAnswerByAssessmentQuestion(
        lessonId: string,
        next: NextFunction,
    ): Promise<[{ answerChoice: string; isCorrect: boolean }] | void> {
        try {
            const lesson = await Lesson.aggregate([
                {
                    $match: { _id: new mongoose.Types.ObjectId(lessonId) },
                },
                {
                    $lookup: {
                        from: 'assessments',
                        let: { assessmentId: '$assessmentId' },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $in: ['$_id', '$$assessmentId'],
                                    },
                                },
                            },
                        ],
                        as: 'assessment',
                    },
                },
                {
                    $project: {
                        assessment: 1,
                        topicId: 1,
                        createdByUserId: 1,
                    },
                },
            ]);
            const assessment = await Assessment.findOne({
                _id: lesson[0]?.assessment[0],
            });
            return assessment?.answerChoices?.filter(
                ({ isCorrect }) => isCorrect === true,
            );
        } catch (e) {
            return next(e);
        }
    }
    public async getLessonsByTopicId(
        topicId: string,
        next: NextFunction,
    ): Promise<LessonData[] | void> {
        try {
            return Lesson.find({ topicId });
        } catch (e) {
            return next(e);
        }
    }
    public async updateLessonById(
        id: string,
        {
            topicId,
            assessmentQuestion,
            answerChoices,
            instruction,
            instructionFile,
            createdByUserId,
        }: IAddLesson,
        next: NextFunction,
    ): Promise<LessonData | void> {
        const session = await mongoose.startSession();
        try {
            await session.startTransaction();
            const existingInstruction = await Instruction.findOne({
                instruction,
            }).session(session);
            let updatedInstruction;
            let newInstruction;
            if (existingInstruction) {
                updatedInstruction = await Instruction.findByIdAndUpdate(
                    existingInstruction.id,
                    { instruction },
                ).session(session);
            } else {
                newInstruction = await Instruction.create([{ instruction }], {
                    session,
                });
            }
            const newAssessment = await Assessment.create(
                [
                    {
                        assessmentQuestion,
                        answerChoices,
                    },
                ],
                { session },
            );
            instructionFile = `https://${process.env.BUCKET}.${process.env.DIGITAL_OCEAN_ENDPOINT}/${instructionFile}`;
            const lessonData = {
                topicId,
                instructionFile,
                instructionId: [
                    updatedInstruction?.id || newInstruction[0]?._id,
                ],
                assessmentId: [newAssessment[0]?.id],
                createdByUserId,
            };
            const lesson = await Lesson.findByIdAndUpdate(id, lessonData, {
                new: true,
            });
            await session.commitTransaction();
            return lesson;
        } catch (e) {
            await session.abortTransaction();
            return next(e);
        } finally {
            session.endSession();
        }
    }
    public async getAll(next: NextFunction): Promise<LessonData[] | void> {
        try {
            return Lesson.aggregate([
                {
                    $lookup: {
                        from: 'topics',
                        let: { topicId: '$topicId' },
                        pipeline: [
                            {
                                $match: {
                                    $expr: { $eq: ['$_id', '$$topicId'] },
                                },
                            },
                        ],
                        as: 'topic',
                    },
                },
                {
                    $lookup: {
                        from: 'instructions',
                        let: { instructionId: '$instructionId' },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $in: ['$_id', '$$instructionId'],
                                    },
                                },
                            },
                        ],
                        as: 'instruction',
                    },
                },
                {
                    $lookup: {
                        from: 'assessments',
                        let: { assessmentId: '$assessmentId' },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $in: ['$_id', '$$assessmentId'],
                                    },
                                },
                            },
                        ],
                        as: 'assessment',
                    },
                },
                {
                    $unwind: {
                        path: '$topic',
                        preserveNullAndEmptyArrays: true,
                    },
                },
                {
                    $project: {
                        topic: `$topic.name`,
                        instruction: 1,
                        instructionFile: 1,
                        assessment: 1,
                        topicId: 1,
                        createdByUserId: 1,
                    },
                },
            ]);
        } catch (e) {
            return next(e);
        }
    }
    public async addNewLesson(
        {
            topicId,
            assessmentQuestion,
            answerChoices,
            instruction,
            instructionFile,
            createdByUserId,
        }: IAddLesson,
        next: NextFunction,
    ): Promise<IAddLesson | void> {
        const session = await mongoose.startSession();
        try {
            await session.startTransaction();
            const newAssessment = await Assessment.create(
                [{ assessmentQuestion, answerChoices }],
                { session },
            );
            instructionFile =
                instructionFile &&
                `https://${process.env.BUCKET}.${process.env.DIGITAL_OCEAN_ENDPOINT}/${instructionFile}`;
            const newInstruction = await Instruction.create([{ instruction }], {
                session,
            });
            if (newInstruction[0]._id && newAssessment[0]._id) {
                const lesson = await Lesson.create(
                    [
                        {
                            topicId,
                            instructionId: [newInstruction[0]._id],
                            instructionFile,
                            assessmentId: [newAssessment[0]._id],
                            createdByUserId,
                        },
                    ],
                    { session },
                );
                const courses = await Course.find({
                    topicIds: { $in: [lesson[0].topicId] },
                }).session(session);
                const courseIds = courses.map(({ _id }) => _id);
                const usersToUpdate = await User.find({
                    'courses.course': { $in: courseIds },
                }).session(session);
                for (const user of usersToUpdate) {
                    for (const course of user?.courses) {
                        const courseObj = await Course.findById(
                            course.course,
                        ).session(session);
                        const status = await userService.getCourseStatus(
                            user._id,
                            courseObj,
                            next,
                        );
                        await User.findOneAndUpdate(
                            {
                                _id: user._id,
                                courses: {
                                    $elemMatch: {
                                        course: new mongoose.Types.ObjectId(
                                            courseObj?._id,
                                        ),
                                    },
                                },
                            },
                            { $set: { 'courses.$.status': status } },
                            { new: true, safe: true },
                        ).session(session);
                    }
                }
                const data = await Lesson.aggregate([
                    {
                        $match: {
                            _id: new mongoose.Types.ObjectId(lesson[0]?._id),
                        },
                    },
                    {
                        $lookup: {
                            from: 'topics',
                            let: { topicId: '$topicId' },
                            pipeline: [
                                {
                                    $match: {
                                        $expr: { $eq: ['$_id', '$$topicId'] },
                                    },
                                },
                            ],
                            as: 'topic',
                        },
                    },
                    {
                        $lookup: {
                            from: 'instructions',
                            let: { instructionId: '$instructionId' },
                            pipeline: [
                                {
                                    $match: {
                                        $expr: {
                                            $in: ['$_id', '$$instructionId'],
                                        },
                                    },
                                },
                            ],
                            as: 'instruction',
                        },
                    },
                    {
                        $lookup: {
                            from: 'assessments',
                            let: { assessmentId: '$assessmentId' },
                            pipeline: [
                                {
                                    $match: {
                                        $expr: {
                                            $in: ['$_id', '$$assessmentId'],
                                        },
                                    },
                                },
                            ],
                            as: 'assessment',
                        },
                    },
                    {
                        $unwind: {
                            path: '$topic',
                            preserveNullAndEmptyArrays: true,
                        },
                    },
                    {
                        $unwind: {
                            path: '$instruction',
                            preserveNullAndEmptyArrays: true,
                        },
                    },
                    {
                        $unwind: {
                            path: '$assessment',
                            preserveNullAndEmptyArrays: true,
                        },
                    },
                    {
                        $project: {
                            topic: `$topic.name`,
                            instructionType: `$instruction.instructionType`,
                            instruction: `$instruction.instruction`,
                            assessmentQuestion: `$assessment.assessmentQuestion`,
                            answerChoices: `$assessment.answerChoices`,
                            topicId: 1,
                            createdByUserId: 1,
                            instructionFile: 1,
                        },
                    },
                ]).session(session);
                const currentTopic = await Topic.findById(data[0].topicId);
                if (!currentTopic.lessonIds.includes(data[0]._id)) {
                    await Topic.findOneAndUpdate(
                        {
                            _id: data[0].topicId,
                        },
                        {
                            $push: {
                                lessonIds: data[0]._id,
                            },
                        },
                        {
                            session,
                        },
                    );
                }
                await session.commitTransaction();
                return data[0];
            }
        } catch (e) {
            await session.abortTransaction();
            return next(e);
        } finally {
            session.endSession();
        }
    }
    public async createBulk(
        lessons: ILesson[],
        next: NextFunction,
    ): Promise<LessonData[] | void> {
        try {
            return Lesson.create(lessons);
        } catch (e) {
            return next(e);
        }
    }
    public async deleteMany(
        query: {
            topicId: string;
        },
        next: NextFunction,
    ): Promise<{ deletedCount: number } | void> {
        try {
            return Lesson.deleteMany(query);
        } catch (e) {
            return next(e);
        }
    }
}
