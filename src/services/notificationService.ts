import { NotificationData } from './../core/models/notificationModel';
import Notification from '../db/models/notification';
import { NextFunction } from 'express';
import mongoose from 'mongoose';
import moment from 'moment';
export default class NotificationService {
    public async getAll(
        next: NextFunction,
    ): Promise<NotificationData[] | void> {
        try {
            const notifications = await Notification.find();
            const userData = notifications.map(
                (note) =>
                    new NotificationData(
                        note._id,
                        note.businessAdminId,
                        note.userId,
                        note.courseId,
                        note.seen,
                        note.text,
                        moment(note.created_at)
                            .format('YYYY-MM-DD HH:mm')
                            .toString(),
                    ),
            );
            return userData;
        } catch (e) {
            return next(e);
        }
    }
    public async getNotificationsByUserId(
        id: string,
        limit: number,
        skip: number,
        isNew: boolean,
        next: NextFunction,
    ): Promise<{
        userNotifications: NotificationData[];
        count: number;
    } | void> {
        try {
            const notifications = await Notification.aggregate([
                {
                    $match: isNew
                        ? {
                              userId: new mongoose.Types.ObjectId(id),
                          }
                        : {
                              seen: isNew,
                              userId: new mongoose.Types.ObjectId(id),
                          },
                },
                {
                    $sort: {
                        created_at: -1,
                    },
                },
                {
                    $limit: limit * (skip + 1),
                },
                {
                    $skip: 0,
                },
            ]);

            const count: number = await Notification.countDocuments(
                isNew
                    ? {
                          userId: id,
                      }
                    : {
                          seen: isNew,
                          userId: id,
                      },
            );
            const userNotifications = notifications.map(
                (note) =>
                    new NotificationData(
                        note._id,
                        note.businessAdminId,
                        note.userId,
                        note.courseId,
                        note.seen,
                        note.text,
                        moment(note.created_at)
                            .format('YYYY-MM-DD HH:mm')
                            .toString(),
                    ),
            );
            return { userNotifications, count };
        } catch (e) {
            return next(e);
        }
    }
    public async addNewNotification(
        { businessAdminId, courseId, userId }: NotificationData,
        next: NextFunction,
    ): Promise<NotificationData | void> {
        try {
            const notification = new Notification({
                businessAdminId,
                userId,
                courseId,
            });
            await notification.save();
            return new NotificationData(
                notification._id,
                notification.businessAdminId,
                notification.userId,
                notification.courseId,
            );
        } catch (e) {
            return next(e);
        }
    }
    public async updateNotification(
        ids: string[],
        next: NextFunction,
    ): Promise<void> {
        try {
            await Notification.updateMany(
                { _id: { $in: ids } },
                { $set: { seen: true } },
                { new: true },
            );
            return;
        } catch (e) {
            return next(e);
        }
    }
}
