import { NextFunction } from 'express';
import Topic from '../db/models/topic';
import Lesson from '../db/models/lesson';
import Course from '../db/models/course';
import { TopicData, ITopic } from '../core/models/topicModel';
import mongoose from 'mongoose';
import InstructionService from './instructionService';
import AssessmentService from './assessmentService';

const instructionService = new InstructionService();
const assessmentService = new AssessmentService();
export default class TopicService {
    public async getAll(next: NextFunction): Promise<ITopic[] | void> {
        try {
            return Topic.aggregate([
                {
                    $lookup: {
                        from: 'lessons',
                        let: { lessonIds: '$lessonIds' },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $in: ['$_id', '$$lessonIds'],
                                    },
                                },
                            },
                            {
                                $lookup: {
                                    from: 'instructions',
                                    let: { instructionId: '$instructionId' },
                                    pipeline: [
                                        {
                                            $match: {
                                                $expr: {
                                                    $in: [
                                                        '$_id',
                                                        '$$instructionId',
                                                    ],
                                                },
                                            },
                                        },
                                    ],
                                    as: 'instruction',
                                },
                            },
                            {
                                $lookup: {
                                    from: 'assessments',
                                    let: { assessmentId: '$assessmentId' },
                                    pipeline: [
                                        {
                                            $match: {
                                                $expr: {
                                                    $in: [
                                                        '$_id',
                                                        '$$assessmentId',
                                                    ],
                                                },
                                            },
                                        },
                                    ],
                                    as: 'assessment',
                                },
                            },
                            {
                                $project: {
                                    instructionType: 0,
                                    instructionId: 0,
                                    assessmentId: 0,
                                },
                            },
                        ],
                        as: 'lessons',
                    },
                },
                {
                    $project: {
                        lessonIds: 0,
                    },
                },
            ]);
        } catch (e) {
            return next(e);
        }
    }
    public async getTopicById(
        id: string,
        next: NextFunction,
    ): Promise<TopicData | void> {
        try {
            const topic = await Topic.findById(id);
            return new TopicData(topic._id, topic.name, topic.createdByUserId);
        } catch (e) {
            return next(e);
        }
    }
    public async deleteTopicById(
        id: string,
        next: NextFunction,
    ): Promise<{ deletedCount: number } | void> {
        const session = await mongoose.startSession();
        try {
            await session.startTransaction();
            const topic = await Topic.findById(id).session(session);
            if (topic) {
                const lessons = await Lesson.find({
                    topicId: topic._id,
                }).session(session);
                if (lessons.length) {
                    for (const lesson of lessons) {
                        await instructionService.deleteInstructionById(
                            lesson?.instructionId[0],
                            next,
                        );
                        await assessmentService.deleteAssessmentById(
                            lesson?.assessmentId[0],
                            next,
                        );
                    }
                }
                await Lesson.deleteMany({ topicId: topic?._id }).session(
                    session,
                );
                const deletedTopic = await Topic.findByIdAndDelete(id).session(
                    session,
                );
                await Course.updateMany(
                    { topicIds: { $in: [id] } },
                    { $pull: { topicIds: id } },
                );
                await session.commitTransaction();
                return deletedTopic;
            }
        } catch (e) {
            await session.abortTransaction();
            return next(e);
        } finally {
            session.endSession();
        }
    }
    public async getTopicByName(
        name: string,
        next: NextFunction,
    ): Promise<TopicData | void> {
        try {
            const topic = await Topic.findOne({ name });
            return topic;
        } catch (e) {
            return next(e);
        }
    }
    public async getTopicsByCreator(
        createdByUserId: string,
        next: NextFunction,
    ): Promise<TopicData[] | void> {
        try {
            const topics = await Topic.find({
                createdByUserId: new mongoose.Types.ObjectId(createdByUserId),
            });
            return topics;
        } catch (e) {
            return next(e);
        }
    }
    public async addNewTopic(
        { name, createdByUserId }: ITopic,
        next: NextFunction,
    ): Promise<TopicData | void> {
        const session = await mongoose.startSession();
        try {
            await session.startTransaction();
            const topic = new Topic({
                name,
                createdByUserId,
            });
            await topic.save();
            await session.commitTransaction();
            return new TopicData(topic._id, topic.name, topic.createdByUserId);
        } catch (e) {
            await session.abortTransaction();
            return next(e);
        } finally {
            session.endSession();
        }
    }
    public async editTopic(
        id: string,
        { name, createdByUserId, lessonIds }: ITopic,
        next: NextFunction,
    ): Promise<TopicData | void> {
        try {
            const topic = await Topic.findByIdAndUpdate(
                id,
                {
                    name,
                    lessonIds,
                    createdByUserId,
                },
                { new: true },
            );
            return topic;
        } catch (e) {
            return next(e);
        }
    }
}
