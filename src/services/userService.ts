import User from '../db/models/user';
import mongoose from 'mongoose';
import { Request } from 'express';
import Lesson from '../db/models/lesson';
import AssessmentResult from '../db/models/assessmentResult';
import { UserData, UserInterface } from '../core/models/userModel';
import { NextFunction } from 'express';
import { CourseStatus } from '../utils/constant';
import { CourseData } from '../core/models/courseModel';
import Course from '../db/models/course';

export default class UserService {
    public async addUserPassword(
        id: string,
        firstName: string,
        lastName: string,
        password: string,
        next: NextFunction,
    ): Promise<UserData | void> {
        try {
            const user = await User.findByIdAndDelete(id);
            const updatedUser = new User({
                firstName: firstName,
                lastName: lastName,
                email: user.email,
                role: user.role,
            });
            updatedUser.setPassword(password);
            await updatedUser.save();
            return new UserData(
                updatedUser._id,
                updatedUser.firstName,
                updatedUser.lastName,
                updatedUser.email,
                updatedUser.courses,
                updatedUser.role,
            );
        } catch (err) {
            return next(err);
        }
    }

    public async getAll(next: NextFunction): Promise<UserData[] | void> {
        try {
            const users = await User.find();
            const userData = users.map(
                (user) =>
                    new UserData(
                        user._id,
                        user.firstName,
                        user.lastName,
                        user.email,
                        user.courses,
                        user.role,
                    ),
            );
            return userData;
        } catch (e) {
            return next(e);
        }
    }

    public async getUserById(
        req: Request,
        id: string,
        next: NextFunction,
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
    ): Promise<UserData | void | any> {
        try {
            const { query } = req;
            const user = await User.aggregate([
                {
                    $match: {
                        _id: new mongoose.Types.ObjectId(id),
                    },
                },
                {
                    $lookup: {
                        from: 'courses',
                        let: { courses: '$courses' },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $in: ['$_id', '$$courses.course'],
                                    },
                                },
                            },
                            {
                                $match: Object.assign(
                                    query.search
                                        ? {
                                              name: {
                                                  $regex: new RegExp(
                                                      query.search.toString(),
                                                  ),
                                                  $options: 'i',
                                              },
                                          }
                                        : {},
                                ),
                            },
                            {
                                $lookup: {
                                    from: 'topics',
                                    let: { topicIds: '$topicIds' },
                                    pipeline: [
                                        {
                                            $match: {
                                                $expr: {
                                                    $in: ['$_id', '$$topicIds'],
                                                },
                                            },
                                        },
                                        {
                                            $lookup: {
                                                from: 'lessons',
                                                let: {
                                                    lessonIds: '$lessonIds',
                                                },
                                                pipeline: [
                                                    {
                                                        $match: {
                                                            $expr: {
                                                                $in: [
                                                                    '$_id',
                                                                    '$$lessonIds',
                                                                ],
                                                            },
                                                        },
                                                    },
                                                    {
                                                        $lookup: {
                                                            from: 'instructions',
                                                            let: {
                                                                instructionId:
                                                                    '$instructionId',
                                                            },
                                                            pipeline: [
                                                                {
                                                                    $match: {
                                                                        $expr: {
                                                                            $in: [
                                                                                '$_id',
                                                                                '$$instructionId',
                                                                            ],
                                                                        },
                                                                    },
                                                                },
                                                            ],
                                                            as: 'instruction',
                                                        },
                                                    },
                                                    {
                                                        $lookup: {
                                                            from: 'assessments',
                                                            let: {
                                                                assessmentId:
                                                                    '$assessmentId',
                                                            },
                                                            pipeline: [
                                                                {
                                                                    $match: {
                                                                        $expr: {
                                                                            $in: [
                                                                                '$_id',
                                                                                '$$assessmentId',
                                                                            ],
                                                                        },
                                                                    },
                                                                },
                                                            ],
                                                            as: 'assessment',
                                                        },
                                                    },
                                                    {
                                                        $project: {
                                                            instructionId: 0,
                                                            assessmentId: 0,
                                                        },
                                                    },
                                                ],
                                                as: 'lessons',
                                            },
                                        },
                                        {
                                            $project: {
                                                lessonIds: 0,
                                            },
                                        },
                                    ],
                                    as: 'topics',
                                },
                            },
                            {
                                $project: {
                                    topicIds: 0,
                                },
                            },
                        ],
                        as: 'courseList',
                    },
                },
                {
                    $project: {
                        userIds: 0,
                        password: 0,
                    },
                },
            ]);
            return { ...user[0], id: user[0]?._id };
        } catch (e) {
            return next(e);
        }
    }

    public async updateUserById(
        id: string,
        user: UserData,
        next: NextFunction,
    ): Promise<UserData | void> {
        try {
            const updatedUser = await User.findByIdAndUpdate(id, user);
            return new UserData(
                updatedUser._id,
                updatedUser.firstName,
                updatedUser.lastName,
                updatedUser.email,
                updatedUser.courses,
                updatedUser.role,
            );
        } catch (e) {
            return next(e);
        }
    }

    public async getUserByEmail(
        email: string,
        next: NextFunction,
    ): Promise<UserInterface | void> {
        try {
            return await User.findOne({ email });
        } catch (e) {
            return next(e);
        }
    }

    public async addNewUser(
        body: {
            firstName: string;
            lastName: string;
            email: string;
            password: string;
            role: string;
        },
        next: NextFunction,
    ): Promise<UserData | void> {
        try {
            const user = new User({
                firstName: body.firstName,
                lastName: body.lastName,
                email: body.email,
                role: body.role,
            });
            user.setPassword(body.password);
            await user.save();
            return new UserData(
                user._id,
                user.firstName,
                user.lastName,
                user.email,
                user.courses,
                user.role,
            );
        } catch (e) {
            return next(e);
        }
    }

    public async getCourseStatus(
        userId: string,
        course: CourseData,
        next: NextFunction,
    ): Promise<string | void> {
        try {
            const courseLessons = course?.topicIds?.length
                ? await Lesson.aggregate([
                      {
                          $match: {
                              $expr: {
                                  $in: ['$topicId', course?.topicIds],
                              },
                          },
                      },
                  ])
                : [];
            const lessonIds = courseLessons.map(({ _id }) => _id);
            const passedLessons = await AssessmentResult.find({
                userId,
                lessonId: { $in: lessonIds },
            });
            if (
                !isNaN(courseLessons?.length) &&
                !isNaN(passedLessons?.length)
            ) {
                if (
                    courseLessons?.length === 0 &&
                    passedLessons?.length === 0
                ) {
                    return CourseStatus.PENDING;
                }
                return passedLessons?.length === courseLessons?.length
                    ? CourseStatus.COMPLETED
                    : CourseStatus.PENDING;
            }
        } catch (e) {
            return next(e);
        }
    }

    public async getPassedLessons(
        userId: string,
        courseId: string,
        next: NextFunction,
    ): Promise<{ passed: number; total: number; courseId: string } | void> {
        try {
            const course = await Course.findById(courseId);
            const courseLessons = course?.topicIds?.length
                ? await Lesson.aggregate([
                      {
                          $match: {
                              $expr: {
                                  $in: ['$topicId', course?.topicIds],
                              },
                          },
                      },
                  ])
                : [];
            const lessonIds = courseLessons.map(({ _id }) => _id);
            let passedLessons = await AssessmentResult.find({
                userId,
                lessonId: { $in: lessonIds },
            });
            passedLessons = passedLessons.reduce((acc, item) => {
                !acc.includes(item.lessonId.toString()) && item.score !== 0
                    ? acc.push(item.lessonId.toString())
                    : null;
                return acc;
            }, []);
            return {
                passed: passedLessons.length,
                total: courseLessons.length,
                courseId: courseId,
            };
        } catch (e) {
            return next(e);
        }
    }
}
